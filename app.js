var express = require('express'),
	path = require('path'),
	config = require('./config'),
	async = require('async'),
	gpio = require('gpio'),
	app = express();

var CHANNEL_1_PIN = gpio.export(2, {
	direction: 'out',
	interval: 200
	}),
    CHANNEL_2_PIN = gpio.export(3, {
		direction: 'out',
		interval: 200
	}),
    CHANNEL_3_PIN = gpio.export(4, {
		direction: 'out',
		interval: 200
	}),
    CHANNEL_4_PIN = gpio.export(17, {
		direction: 'out',
		interval: 200
	});

app.set('port', process.env.PORT || 3000);

app.use('/', express.static(__dirname + '/public'));

function delayPinWrite(pin, value, callback) {
	setTimeout(function() {
		gpio.write(pin, value, callback);
	}, config.RELAY_TIMEOUT);
}

app.get("/api/ping", function(req, res) {
	res.json("pong");
});

app.post("/api/inputSelect/channel1", function(req, res) {
	CHANNEL_1_PIN.set();
	setTimeout(500);
	CHANNEL_1_PIN.reset();
	/*async.series([
		function(callback) {
			// Open pin for output
			gpio.open(CHANNEL_1_PIN, "output", callback);
		},
		function(callback) {
			// Turn the relay on
			gpio.write(CHANNEL_1_PIN, config.RELAY_ON, callback);
		},
		function(callback) {
			// Turn the relay off after delay to simulate button press
			delayPinWrite(CHANNEL_1_PIN, config.RELAY_OFF, callback);
		},
		function(err, results) {
			setTimeout(function() {
				// Close pin from further writing
				gpio.close(CHANNEL_1_PIN);
				// Return json
				res.json("ok");
			}, config.RELAY_TIMEOUT);
		}
	]);*/
});

app.post("/api/inputSelect/channel2", function(req, res) {
	CHANNEL_2_PIN.set();
	setTimeout(500);
	CHANNEL_2_PIN.reset();
	/*async.series([
		function(callback) {
			// Open pin for output
			gpio.open(CHANNEL_2_PIN, "output", callback);
		},
		function(callback) {
			// Turn the relay on
			gpio.write(CHANNEL_2_PIN, config.RELAY_ON, callback);
		},
		function(callback) {
			// Turn the relay off after delay to simulate button press
			delayPinWrite(CHANNEL_2_PIN, config.RELAY_OFF, callback);
		},
		function(err, results) {
			setTimeout(function() {
				// Close pin from further writing
				gpio.close(CHANNEL_2_PIN);
				// Return json
				res.json("ok");
			}, config.RELAY_TIMEOUT);
		}
	]);*/
});

app.post("/api/inputSelect/channel3", function(req, res) {
	CHANNEL_3_PIN.set();
	setTimeout(500);
	CHANNEL_3_PIN.reset();
	/*async.series([
		function(callback) {
			// Open pin for output
			gpio.open(CHANNEL_3_PIN, "output", callback);
		},
		function(callback) {
			// Turn the relay on
			gpio.write(CHANNEL_3_PIN, config.RELAY_ON, callback);
		},
		function(callback) {
			// Turn the relay off after delay to simulate button press
			delayPinWrite(CHANNEL_3_PIN, config.RELAY_OFF, callback);
		},
		function(err, results) {
			setTimeout(function() {
				// Close pin from further writing
				gpio.close(CHANNEL_3_PIN);
				// Return json
				res.json("ok");
			}, config.RELAY_TIMEOUT);
		}
	]);*/
});

app.post("/api/inputSelect/channel4", function(req, res) {
	CHANNEL_4_PIN.set();
	setTimeout(500);
	CHANNEL_4_PIN.reset();
	/*async.series([
		function(callback) {
			// Open pin for output
			gpio.open(CHANNEL_4_PIN, "output", callback);
		},
		function(callback) {
			// Turn the relay on
			gpio.write(CHANNEL_4_PIN, config.RELAY_ON, callback);
		},
		function(callback) {
			// Turn the relay off after delay to simulate button press
			delayPinWrite(CHANNEL_4_PIN, config.RELAY_OFF, callback);
		},
		function(err, results) {
			setTimeout(function() {
				// Close pin from further writing
				gpio.close(CHANNEL_4_PIN);
				// Return json
				res.json("ok");
			}, config.RELAY_TIMEOUT);
		}
	]);*/
});

app.listen(app.get('port'));
