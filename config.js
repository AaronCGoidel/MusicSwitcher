var config = {};

config.CHANNEL_1_PIN = 11;
config.CHANNEL_2_PIN = 11;
config.CHANNEL_3_PIN = 11;
config.CHANNEL_4_PIN = 11;

config.RELAY_ON = 0;
config.RELAY_OFF = 1;
config.RELAY_TIMEOUT = 500;

module.exports = config;
